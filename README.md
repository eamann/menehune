# Menehune

jQuery wrapper for WebWorkers.

## Getting Started
Download the [production version][min] or the [development version][max].

[min]: https://raw.github.com/Eric/menehune/master/dist/Menehune.min.js
[max]: https://raw.github.com/Eric/menehune/master/dist/Menehune.js

In your web page:

```html
<script src="jquery.js"></script>
<script src="dist/Menehune.min.js"></script>
<script>
jQuery(function($) {
  $.awesome(); // "awesome"
});
</script>
```

## Documentation
_(Coming soon)_

## Examples
_(Coming soon)_

## Release History
_(Nothing yet)_
